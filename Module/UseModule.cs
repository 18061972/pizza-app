﻿using PizzaConsoleApp.PizzaProductPojo;
using System.Diagnostics;

namespace PizzaConsoleApp.ModuleInterfaces
{
    class UseModule : ICustomerModule,IPizzaModule
    {
        Pizza[] AllPizza = new Pizza[20];
        int [] order_pizza= new int[10];
        int totalPizza = 6;
        int orderedPizzaByUser=0;
        public UseModule()
        {
            AllPizza[0] = new Pizza(1, "CreamyTomatoPasta Pizza", 100);
            AllPizza[1] = new Pizza(2, "Paneer Paratha Pizza   ", 200);
            AllPizza[2] = new Pizza(3, "Fresh Veggie           ", 200);
            AllPizza[3] = new Pizza(4, "Margherita             ", 300);
            AllPizza[4] = new Pizza(5, "Veggie Paradise        ", 500);
            AllPizza[5] = new Pizza(6, "The Unthinkable Pizza  ", 400);
        }
        
        public Pizza[] GetAllPizza()
        {
            return AllPizza;
        }

        public bool OrderPizza(int pizzaId)
        {

            for (int i = 0; i < totalPizza; i++)
            {
                if (AllPizza[i].pizzaId == pizzaId)
                {
                    order_pizza[orderedPizzaByUser] = pizzaId;
                    orderedPizzaByUser++;
                    return true;
                }
            }
            return false;
        }

        public bool CancelPizza()
        {
            if (orderedPizzaByUser > 0)
            {
                for (int i = 0; i < orderedPizzaByUser; i++)
                {
                    order_pizza[i] = 0;
                    orderedPizzaByUser = 0;
                }
                return true;
            }
            return false;
        }

        public double GetBill()
        {
            double sum=0;
            for(int i = 0; i < orderedPizzaByUser; i++)
            {
                for(int j=0; j < totalPizza; j++)
                {

                    if(AllPizza[j].pizzaId==order_pizza[i])
                    {
                        sum = sum + AllPizza[j].pizzaPrice;
                    }
                }
            }
            return sum;
        }

        public bool AddNewPizza(Pizza addPizza)
        {
            if (totalPizza != 20)
            {
                AllPizza[totalPizza] = addPizza;
                totalPizza++;
                return true;
            }
            return false;
        }

        public bool DeletePizza(int delPizza)
        {
            for(int i = 0; i < totalPizza; i++)
            {
                if (AllPizza[i].pizzaId == delPizza)
                {
                    int j;
                    for(j= i; j < totalPizza-1; j++)
                    {
                        AllPizza[j]= AllPizza[j+1];
                    }
                    AllPizza[j] = null;
                    totalPizza--;
                    return true;
                }
            }
            return false;
        }

        public void UpdatePizzaPrice(int pizzaId,int pizzaPrice)
        {
            for (int i = 0; i < totalPizza; i++)
            {
                if (AllPizza[i].pizzaId == pizzaId)
                {
                    AllPizza[i].pizzaPrice = pizzaPrice;
                }
            }
        }


        public bool GetIdValid(int updatePizzaId)
        {
            for (int i = 0; i < totalPizza; i++)
            {
                if (AllPizza[i].pizzaId == updatePizzaId)
                {
                    return true;
                }
            }
            return false;
        }

        public void UpdatePizzaName(int pizzaId, string? newName)
        {
            for (int i = 0; i < totalPizza; i++)
            {
                if (AllPizza[i].pizzaId == pizzaId)
                {
                    AllPizza[i].pizzaName = newName;
                }
            }
        }

    }
}
