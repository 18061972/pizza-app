﻿using PizzaConsoleApp.PizzaProductPojo;

namespace PizzaConsoleApp.ModuleInterfaces
{
    interface ICustomerModule
    {
        bool CancelPizza();
        Pizza[] GetAllPizza();
        double GetBill();
        bool OrderPizza(int pizzaId);
    }

    interface IPizzaModule
    {
        bool AddNewPizza(Pizza add_pizza);
        bool DeletePizza(int del_pizza);
        void UpdatePizzaName(int update_id, string? update_name);
        void UpdatePizzaPrice(int id, int price);
        Pizza[] GetAllPizza();
        bool GetIdValid(int update_id);
    }
}
