﻿namespace PizzaConsoleApp.PizzaProductPojo
{
    class Pizza
    {
        public int pizzaId { get; set; }
        public string ?pizzaName { get; set; }
        public int pizzaPrice { get; set; }

        public Pizza(int pizzaId, string pizzaName, int pizzaPrice)
        {
            this.pizzaId = pizzaId;
            this.pizzaName = pizzaName;
            this.pizzaPrice = pizzaPrice;
        }

        //OverRide To String
        public override string ToString()
        {
            return ($"\t\t{pizzaId}\t{pizzaName}\t\tRs{pizzaPrice}/-");
        }
    }
}
